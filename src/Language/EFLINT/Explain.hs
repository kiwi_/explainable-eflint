{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BlockArguments #-}
module Language.EFLINT.Explain(Explanation (Explanation), search_trace, search_derivations, sub_var, 
                               perform_subs, in_trans, traverse_store, strip_doms, get_elem_from_args, term_to_elem, 
                               get_dom_from_term, get_lit_from_term, int_value, bool_value, get_spec_from_cfg,
                               search_trace_for_term, search_derived_clause, find_expl, tagged_to_term, 
                               traverse_store_find_duties, get_args, concating, concatingvis, op_compliance) where
import Language.EFLINT.Explorer (Label, Explorer)
import Language.EFLINT.Spec
import Language.EFLINT.Interpreter (Program (Program), Config (Config), ex_triggers, Output)
import Language.Explorer.Monadic (Ref, getTrace)
import Language.EFLINT.State (TransInfo (TransInfo), Assignment (HoldsTrue, HoldsFalse, Unknown), Store, QueryRes (QueryFailure, QuerySuccess))
import Data.Maybe (fromJust)
import qualified Data.Map as M
import Data.Map (keys)
import Language.EFLINT.Options
import Language.EFLINT.Print (ppTerm, ppPhrase, ppDecl)

data Explanation = Explanation (Phrase, Ref, Ref, Maybe Decl)
                 deriving (Eq, Show, Read)

{--
  TESTS WHERE NO EXPLANATION IS DISPLAYED OR WRONGLY DISPLAYED: 
    - acts/*
    - conditioned-by/*
    - derivation-rules/*                       note: explanation doesn't work properly on transitivity and symmetry.
    - expressions/*

  Enter phrase (for now, only consider queries). If the instance exists in the knowledge base, search in the trace at what
  step it was created (in the case of effects: by what construct) and incorporate that in the end result. Using the spec, 
  list conditions (search specifically for the holds when keyword in the spec). Search for this condition in the knowledge
  base, then determine again in the trace where it is attributed a value. Use these pieces for now to give an explanation.

  DONE: If a query fails, there are two possibilities: either it is in the knowledge base, or it is not. If it is, then we look 
  for the transition in which the instance was set to False. Otherwise, we simply display no explanation at all.

  DONE: how can i get all this information?
  - the phrase:
  - the knowledge base: is the inputmap?
  - the trace: getTrace program m config output
    - the program:
    - m:
    - config:
    - output:
  - the spec: get using context like in sem_phrase

  DONE: once I have this information, how can I use it?
  - the phrase: just display it the way it was entered in the REPL.
  - the knowledge base: display nothing, only look for the truth (already being done currently).
  - the trace: only display the transition (old state id => new state id) at which the instance was created.
  - the spec: only display the relevant declaration.

  DONE: also substitution needs to be done to display meaningful data (using every_possible_subs?).
  DONE: when a fact is created with different arguments, the explanation shows the first instance where the fact is
        created. Here, the argument may be different. e.g. in tests/postconditions/counter_nd_create.eflint.
          - How do i even see the value of the created fact?
  DONE: derived facts
  DONE: compliance reports on duties

  Facts that have conditions: search whether condition is fulfilled.

  DONE: in the case of searching for successful query using postulation.
  | otherwise = traverse_store tags map (el, d)
  CQuery (Present (Tag (StringLit "Bob") "person")) search for CCreate [] (Tag (StringLit "Bob") "person")
  if by postulation: if phrase is found, return the phrase and the two state ids. otherwise return nothing.
  
  example result for postulation:
  #3 > ?person(Alice)
  query successful:
    transition: #2 => #3
    phrase: +person(Alice). -- CCreate ["Alice"] "person"
    by postulation.

  example result for effect:
  #6 > ?adoptive-parent(Alice, Bob)
  query successful:
    transition: #5 => #6
    phrase: sign-adoption-papers(Eve, Alice, Bob).
    by effect:
        Act sign-adoption-papers
            Actor authority("Eve")
            Recipient person("Alice")
            Related to person("Bob")
            Creates duty-to-stay-eligible(person("Alice"), authority("Eve"), person("Bob")), 
                    ->adoptive-parent(person("Alice"), person("Bob")), is-adopted(person("Bob"))
            Holds when eligible(person("Alice"))

  why does this happen? not in scope of research
  #13 > ?person(Bob). ?person(Alice)
  query successful:
  transition: #3 => #4
  phrase: +person("Bob")
  by postulation.
  query successful:
  transition: #3 => #4
  phrase: +person("Bob")
  by postulation.

  DONE: elem has product of tagged which has elem etc etc write something
  
  DONE: how to explain negation queries? something that isn't in the trace. probably not necessary as it is trivial. 
  UNLESS IT GOT TERMINATED. should i then also show the creation of the instance?
  ?!person(Chloe)
  query successful
  *this has been implemented, but queries like ?!!person(Alice) don't show explanations. 
  but yeah nobody would do that, right? :)
--}
search_trace :: Bool -> Phrase -> [((Ref, Config), (Label, [Output]), (Ref, Config))] -> Explanation
search_trace success p [] = Explanation (p, 0, 0, Nothing)
search_trace success p (st:tr) = case st of
  ((r1, c1), ((im, pr), ou), (r2, c2)) -> case p of              
    PQuery(Not(Violated te')) -> case dutyexpl te' of 
      Explanation (vp, vr1, vr2, vdecl) -> 
        if vr1 > 0 then dutyexpl te' else (search_trace True (PQuery te') (st:tr))
    PQuery(Violated te') -> case dutyexpl te' of 
      Explanation (vp, vr1, vr2, vdecl) -> 
        if vr1 > 0 then dutyexpl te' else (search_trace True (PQuery te') (st:tr))
    otherwise -> case pr of
      Program phrase -> case phrase of
        PTrigger vs te -> case te of
          App d args -> case decl of
            Just (TypeSpec kind _ _ _ _ _ g) -> case p of
              PQuery t -> case in_trans success t $ ex_triggers ou of
                Just True -> Explanation(phrase, r1, r2, Just (TypeDecl d $ fromJust sub_decl))
                otherwise -> search_trace success p tr
              otherwise -> search_trace success p tr
            Nothing -> search_trace success p tr
            where 
              sub_decl = case decl of
                Just (TypeSpec a b c d e f g) -> 
                  Just (TypeSpec sub_kind (Products sub_vars) (perform_subs c binds) d sub_derivs f sub_conds)
                  where sub_conds = map (\cond -> perform_subs cond binds) g
                Nothing -> decl
              sub_derivs = case decl of 
                Just (TypeSpec _ _ _ _ derivs _ _) -> map (\deriv -> case deriv of
                  Dv vars te' -> Dv vars $ perform_subs te' binds
                  HoldsWhen te' -> HoldsWhen $ perform_subs te' binds) derivs
                Nothing -> []
              sub_kind = case decl of 
                Just (TypeSpec kind _ _ _ _ _ _) -> case kind of
                  Fact fs -> Fact fs
                  Act (ActSpec efs sys b) -> Act (ActSpec (sub_efs efs) (sub_sys sys) b)
                  Duty ds -> case ds of 
                    DutySpec ss strs css tes -> 
                      Duty (DutySpec ss strs css (map (\te' -> perform_subs te' binds) tes))
                  Event (EventSpec efs sys) -> Event (EventSpec (sub_efs efs) (sub_sys sys))
                  where sub_efs efs = map (\ef -> case ef of 
                          CAll vars te' -> CAll vars $ perform_subs te' binds
                          TAll vars te' -> TAll vars $ perform_subs te' binds
                          OAll vars te' -> OAll vars $ perform_subs te' binds) efs
                        sub_sys sys = map (\sy -> case sy of 
                          Sync vars te' -> Sync vars $ perform_subs te' binds) sys
                Nothing -> Fact (FactSpec False False)
              binds = zip dom dutyargs
              dom = case decl of 
                Just (TypeSpec _ dom _ _ _ _ _) -> case dom of
                  Products vars -> vars
                  otherwise -> []
                Nothing -> []
              sub_vars = map (\bind -> sub_var bind) binds
              dutyargs = get_args te
              spc = get_spec_from_cfg c2 
              decl = find_decl spc d
          otherwise -> search_trace success p tr
        Create vs te -> process_query te
        Terminate vs te -> process_query te 
        Obfuscate vs te -> process_query te 
        otherwise -> search_trace success p tr
        where 
          process_query te = case p of
            PQuery (Not trm)
              | get_lit_from_term trm == get_lit_from_term te && get_dom_from_term trm == get_dom_from_term te
              -> Explanation (phrase, r1, r2, Nothing)
            PQuery trm
              | get_lit_from_term trm == get_lit_from_term te && get_dom_from_term trm == get_dom_from_term te
              -> Explanation (phrase, r1, r2, Nothing)
            otherwise -> search_trace success p tr
      otherwise -> search_trace success p tr
    where 
      dutyexpls te' = map (\cond -> search_trace True (PQuery(cond)) (st:tr)) $ sub_dutyconds te'
      dutyexpl te' = get_latest_expl $ dutyexpls te'
      sub_dutyconds te' = case dutydecl te' of 
        Just (TypeSpec kind _ _ _ _ _ _) -> case kind of
          Duty ds -> case ds of 
            DutySpec ss strs css tes -> (map (\te'' -> perform_subs te'' $ dutybinds te') tes)
          otherwise -> []
        Nothing -> []
      sub_dutyvars te' = map (\bind -> sub_var bind) $ dutybinds te'
      dutybinds te' = zip (dutydom te') $ dutyargs te'
      dutydom te' = case dutydecl te' of 
        Just (TypeSpec _ dom _ _ _ _ _) -> case dom of
          Products vars -> vars
          otherwise -> []
        Nothing -> []
      dutyargs te' = get_args te'
      dutydecl te' = find_decl spc $ qd te'
      spc = get_spec_from_cfg c2
      qd te' = get_dom_from_term te'

{-- search the derivation in the store to find out the which conditions are true. then search when those conditions
became true in the trace. return explanation containing the phrase that made the condition true, the transition
where it became true, and the declaration of the derived fact (so not the one at the transition where the conditional
became true). --}
search_derivations :: Bool -> Phrase -> [((Ref, Config), (Label, [Output]), (Ref, Config))] -> Explanation
search_derivations success p [] = Explanation (p, 0, 0, Nothing)
search_derivations success p (st:tr) = case st of
  ((r1, c1), ((im, pr), ou), (r2, c2)) -> case p of
    PQuery (Not trm) -> case (find_expl (not success) explanations) of
      Explanation(dp, dr1, dr2, ddecl) -> Explanation(dp, dr1, dr2, Just (TypeDecl d $ fromJust sub_decl)) 
      where 
        explanations = map (\deriv -> case deriv of
          Dv vars te' -> search_derived_clause (not success) (PQuery te') (st:tr)
          HoldsWhen te' -> search_derived_clause (not success) (PQuery te') (st:tr)) sub_derivs
    PQuery trm -> case (find_expl success explanations) of
      Explanation(dp, dr1, dr2, ddecl) -> Explanation(dp, dr1, dr2, Just (TypeDecl d $ fromJust sub_decl))
      where 
        explanations = map (\deriv -> case deriv of
          Dv vars te' -> search_derived_clause success (PQuery te') (st:tr)
          HoldsWhen te' -> search_derived_clause success (PQuery te') (st:tr)) sub_derivs
    otherwise -> Explanation (p, 0, 0, Nothing)
    where 
      sub_decl = case decl of
        Nothing -> Nothing
        Just (TypeSpec a b c x e f g) -> 
          Just (TypeSpec sub_kind (Products sub_vars) (perform_subs c binds) x sub_derivs f sub_conds)
          where sub_conds = map (\cond -> perform_subs cond binds) g
      sub_kind = case decl of 
        Just (TypeSpec kind _ _ _ _ _ _) -> case kind of
          Fact fs -> Fact fs
          Act (ActSpec efs sys b) -> Act (ActSpec (sub_efs efs) (sub_sys sys) b)
          Duty ds -> case ds of 
            DutySpec ss strs css tes -> 
              Duty (DutySpec ss strs css (map (\te' -> perform_subs te' binds) tes))
          Event (EventSpec efs sys) -> Event (EventSpec (sub_efs efs) (sub_sys sys))
          where sub_efs efs = map (\ef -> case ef of 
                  CAll vars te' -> CAll vars $ perform_subs te' binds
                  TAll vars te' -> TAll vars $ perform_subs te' binds
                  OAll vars te' -> OAll vars $ perform_subs te' binds) efs
                sub_sys sys = map (\sy -> case sy of 
                  Sync vars te' -> Sync vars $ perform_subs te' binds) sys
        -- temporary placeholder to avoid throwing errors. should probably use nothing
        Nothing -> Fact (FactSpec False False) 
      sub_derivs = case decl of 
        Just (TypeSpec _ _ _ _ derivs _ _) -> map (\deriv -> case deriv of
          Dv vars te' -> Dv vars $ perform_subs te' binds
          HoldsWhen te' -> HoldsWhen $ perform_subs te' binds) derivs
        Nothing -> []
      sub_vars = map (\bind -> sub_var bind) binds
      binds = zip dom args
      dom = case decl of 
        Just (TypeSpec _ dom _ _ _ _ _) -> case dom of
          Products vars -> vars
          otherwise -> []
        Nothing -> []
      args = get_args t
      decl = find_decl spc d
      spc = get_spec_from_cfg c2 
      d = get_dom_from_term t
      t = case p of
        PQuery (Not te) -> te
        PQuery te -> te
        otherwise -> t -- temporary placeholder to avoid throwing errors. should probably use nothing

search_derived_clause :: Bool -> Phrase -> [((Ref, Config), (Label, [Output]), (Ref, Config))] -> Explanation
search_derived_clause success p [] = Explanation(p, 0, 0, Nothing)
search_derived_clause success p (st:tr) = case st of
  ((r1, c1), ((im, pr), ou), (r2, c2)) -> case pr of 
    Program phrase -> case p of
      PQuery trm -> case trm of
        -- generally, both terms should exist in the trace otherwise we cannot give a meaningful explanation.
        And te te' -> process_and_two_terms te te'
        Or te te' -> case phrase of
          -- when we have a query with an or clause, we check whether one of the terms appears in the trace.
          -- derivations are one level deep here.
          PTrigger vs term -> case in_trans success te (ex_triggers ou) of
            Nothing -> case in_trans success te' (ex_triggers ou) of
              Nothing -> search_derived_clause success p tr
              Just False -> search_derived_clause success (PQuery te) tr
              Just True -> Explanation(phrase, r1, r2, Nothing)
            Just False -> search_derived_clause success (PQuery te') (st:tr)
            Just True -> Explanation(phrase, r1, r2, Nothing)
          -- when we encounter any other keyword (create, terminate etc.), we check whether the literals 
          -- in the query term and phrase term. derivations are again one level deep
          otherwise -> case search_trace_for_term success te phrase of
            Nothing -> case search_trace_for_term success te' phrase of
              Nothing -> search_derived_clause success p tr
              Just False -> search_derived_clause success (PQuery te) tr
              Just True -> Explanation(phrase, r1, r2, Nothing)
            Just False -> search_derived_clause success (PQuery te') (st:tr)
            Just True -> Explanation(phrase, r1, r2, Nothing)
        Leq te te' -> process_and_two_terms te te'
        Geq te te' -> process_and_two_terms te te'
        Ge te te' -> process_and_two_terms te te'
        Le te te' -> process_and_two_terms te te'
        Sub te te' -> process_and_two_terms te te'
        Add te te' -> process_and_two_terms te te'
        Mult te te' -> process_and_two_terms te te'
        Mod te te' -> process_and_two_terms te te'
        Div te te' -> process_and_two_terms te te'
        Eq te te' -> process_and_two_terms te te'
        Neq te te' -> process_and_two_terms te te'
        When te te' -> process_and_two_terms te te'

        -- other terms make use of vars, which this implementation does not account for when giving explanations.
        otherwise -> case lit_trm of 
          -- for now, we just look if the dom exists in the trace and give an explanation for that. May result in
          -- wrong explanations, for example if the found dom has args that are different from the ones in the query.
          -- e.g., something like if ?person(Alice) would point to +person(Bob) (it doesn't, but this is the idea).
          App s (Right mods) -> search_just_dom s
          App s (Left ((Ref v):tes)) -> search_just_dom s
          otherwise -> case phrase of
            PTrigger vs term -> case in_trans success trm (ex_triggers ou) of
              Nothing -> search_derived_clause success p tr
              Just False -> Explanation(phrase, 0, 0, Nothing)
              Just True -> Explanation(phrase, r1, r2, Nothing)
            otherwise -> case search_trace_for_term success trm phrase of
              Nothing -> search_derived_clause success p tr
              Just False -> Explanation(phrase, 0, 0, Nothing)
              Just True -> Explanation(phrase, r1, r2, Nothing)
        where
          search_just_dom :: DomId -> Explanation
          search_just_dom s = case phrase of
            PTrigger vs te -> in_trans_dom s (ex_triggers ou)
              where
                in_trans_dom :: DomId -> [TransInfo] -> Explanation 
                in_trans_dom s' [] = search_derived_clause success p tr
                in_trans_dom s' (trans:transs) = case trans of 
                  TransInfo x0 store b ma tis -> case traverse_store_just_dom (keys store) store s' of
                    True -> -- should this always display an explanation where it was edited last?
                      if success then Explanation(phrase, r1, r2, Nothing) else Explanation(phrase, 0,0,Nothing)
                    False -> in_trans_dom s' transs
            Create vs te -> case (get_dom_from_term lit_trm) == (get_dom_from_term te) of 
              True -> Explanation(phrase, r1, r2, Nothing)
              False -> search_derived_clause success p tr
            Terminate vs te -> case (get_dom_from_term lit_trm) == (get_dom_from_term te) of 
              True -> Explanation(phrase, r1, r2, Nothing)
              False -> search_derived_clause success p tr
            Obfuscate vs te -> case (get_dom_from_term lit_trm) == (get_dom_from_term te) of 
              True -> Explanation(phrase, r1, r2, Nothing)
              False -> search_derived_clause success p tr
            otherwise -> search_derived_clause success p tr 
          lit_trm = get_lit_from_term trm
          process_and_two_terms te te' = case phrase of
            PTrigger vs term -> case in_trans success te (ex_triggers ou) of
              Nothing -> case in_trans success te' (ex_triggers ou) of
                Nothing -> search_derived_clause success p tr
                Just False -> case success of 
                  False -> search_derived_clause success (PQuery te) tr
                  True -> Explanation(phrase, 0, 0, Nothing)
                Just True -> case search_derived_clause success (PQuery te) tr of
                  Explanation (p1, ref1, ref2, decl) -> case ref1 > r1 of
                    False -> Explanation (phrase, r1, r2, Nothing)
                    True -> Explanation (p1, ref1, ref2 , decl)
              Just False -> case success of 
                  False -> search_derived_clause success (PQuery te') tr
                  True -> Explanation(phrase, 0, 0, Nothing)
              Just True -> case search_derived_clause success (PQuery te') tr of
                  Explanation (p1, ref1, ref2, decl) -> case ref1 > r1 of 
                    False -> Explanation(phrase, r1, r2, Nothing)
                    True -> Explanation (p1, ref1, ref2, decl)
            otherwise -> case search_trace_for_term success te phrase of
              Nothing -> case search_trace_for_term success te' phrase of
                Nothing -> search_derived_clause success p tr
                Just False -> case success of 
                  False -> search_derived_clause success (PQuery te) tr
                  True -> Explanation(phrase, 0, 0, Nothing)
                Just True -> case search_derived_clause success (PQuery te) tr of
                  Explanation (p1, ref1, ref2, decl) -> case ref1 > r1 of
                    False -> Explanation(phrase, r1, r2, Nothing)
                    True -> Explanation(phrase, ref1, ref2, Nothing)
              Just False -> case success of 
                  False -> search_derived_clause success (PQuery te') tr
                  True -> Explanation(phrase, 0, 0, Nothing)
              Just True -> case search_derived_clause success (PQuery te') tr of
                  Explanation (p1, ref1, ref2, decl) -> case ref1 > r1 of 
                    False -> Explanation(phrase, r1, r2, Nothing)
                    True -> Explanation (p1, ref1, ref2, decl)
      otherwise -> Explanation(p,0,0,Nothing) -- temporary placeholder to avoid throwing errors. should probably use nothing
    otherwise -> search_derived_clause success p tr

find_expl :: Bool -> [Explanation] -> Explanation
find_expl success [] = Explanation(PSkip, 0, 0, Nothing)
find_expl success (e:es) = case e of 
  Explanation(p, ref1, ref2, d)
    | ref2 == 0 -> find_expl success es
    | otherwise -> Explanation(p, ref1, ref2, d)

get_latest_expl :: [Explanation] -> Explanation
get_latest_expl [] = Explanation(PSkip, 0, 0, Nothing)
get_latest_expl (x:[]) = x
get_latest_expl (Explanation(x0,x1,x2,x3):Explanation(y0,y1,y2,y3):z) = case y2 > x2 of 
  True -> get_latest_expl (Explanation(y0,y1,y2,y3):z)
  False -> get_latest_expl (Explanation(x0,x1,x2,x3):z)

get_args :: Term -> [Term]
get_args t = case t of
  Not te -> get_args t
  And te te' -> get_args te ++ get_args te'
  Or te te' -> get_args te ++ get_args te'
  Leq te te' -> get_args te ++ get_args te'
  Geq te te' -> get_args te ++ get_args te'
  Ge te te' -> get_args te ++ get_args te'
  Le te te' -> get_args te ++ get_args te'
  Sub te te' -> get_args te ++ get_args te'
  Add te te' -> get_args te ++ get_args te'
  Mult te te' -> get_args te ++ get_args te'
  Mod te te' -> get_args te ++ get_args te'
  Div te te' -> get_args te ++ get_args te'
  Eq te te' -> get_args te ++ get_args te'
  Neq te te' -> get_args te ++ get_args te'
  Exists vars te -> get_args te
  Forall vars te -> get_args te
  Count vars te -> get_args te
  Sum vars te -> get_args te
  Max vars te -> get_args te
  Min vars te -> get_args te
  When te te' -> get_args te'
  Present te -> get_args te
  Violated te -> get_args te
  Enabled te -> get_args te
  Project te var -> get_args te
  Tag te s -> get_args te
  Untag te -> get_args te
  App s e -> case e of 
    Left tes -> tes
    Right mods -> [t]
  otherwise -> [t]

search_trace_for_term :: Bool -> Term -> Phrase -> Maybe Bool
search_trace_for_term success trm p = case p of
  Create vs te -> process_term te success
  Terminate vs te -> process_term te (not success)
  Obfuscate vs te -> process_term te (not success)
  otherwise -> Nothing
  where 
    process_term te succeeded = case te of
      Not t -> 
        case get_lit_from_term t == get_lit_from_term trm && get_dom_from_term t == get_dom_from_term trm of
          True -> Just (not succeeded)
          otherwise -> Nothing
      otherwise ->
        case get_lit_from_term te == get_lit_from_term trm && get_dom_from_term te == get_dom_from_term trm of
          True -> Just succeeded
          otherwise -> Nothing

sub_var :: (Var, Term) -> Var
sub_var (v,t) = case t of
  BoolLit b -> case v of { Var d str -> Var d ("=\""++(show b)++"\"") }
  IntLit n -> case v of { Var d str -> Var d ("=\""++(show n)++"\"") }
  StringLit s -> case v of { Var d str -> Var d ("=\""++s++"\"") }
  otherwise -> v

{--
te = 
  [HoldsWhen
    (Or
      (Present
        (App
          "adoptive-parent"
          (Right
            [Rename (Var "parent" []) (Ref (Var "parent" [])), Rename (Var "child" []) (Ref (Var "child" []))]
          )
        )
      )
      (Present
        (App
          "natural-parent"
          (Right
            [Rename (Var "parent" []) (Ref (Var "parent" [])), Rename (Var "child" []) (Ref (Var "child" []))]
          )
        )
      )
    )
  ]
te' = App "legal-parent" (Left [StringLit "Alice",StringLit "Bob"])
put left stringlit alice in place of right var parent ref var parent
--}
perform_subs :: Term -> [(Var, Term)] -> Term
perform_subs te binds = case te of
  Not te' -> Not (perform_subs te' binds)
  And te' te2 -> And (perform_subs te' binds) (perform_subs te2 binds)
  Or te' te2 -> Or (perform_subs te' binds) (perform_subs te2 binds)
  Leq te' te2 -> Leq (perform_subs te' binds) (perform_subs te2 binds)
  Geq te' te2 -> Geq (perform_subs te' binds) (perform_subs te2 binds)
  Ge te' te2 -> Ge (perform_subs te' binds) (perform_subs te2 binds)
  Le te' te2 -> Le (perform_subs te' binds) (perform_subs te2 binds)
  Sub te' te2 -> Sub (perform_subs te' binds) (perform_subs te2 binds)
  Add te' te2 -> Add (perform_subs te' binds) (perform_subs te2 binds)
  Mult te' te2 -> Mult (perform_subs te' binds) (perform_subs te2 binds)
  Mod te' te2 -> Mod (perform_subs te' binds) (perform_subs te2 binds)
  Div te' te2 -> Div (perform_subs te' binds) (perform_subs te2 binds)
  Eq te' te2 -> Eq (perform_subs te' binds) (perform_subs te2 binds)
  Neq te' te2 -> Neq (perform_subs te' binds) (perform_subs te2 binds)
  Exists vars te' -> Exists vars (perform_subs te' binds) --dunno here
  Forall vars te' -> Forall vars (perform_subs te' binds) --dunno here
  Count vars te' -> Count vars (perform_subs te' binds) --dunno here
  Sum vars te' -> Sum vars (perform_subs te' binds) --dunno here
  Max vars te' -> Max vars (perform_subs te' binds) --dunno here
  Min vars te' -> Min vars (perform_subs te' binds) --dunno here
  When te' te2 -> When (perform_subs te' binds) (perform_subs te2 binds)
  Present te' -> Present (perform_subs te' binds)
  Violated te' -> (perform_subs te' binds)
  Enabled te' -> (perform_subs te' binds)
  Project te' var -> Project (perform_subs te' binds) var --dunno here
  Tag te' s -> Tag (perform_subs te' binds) s
  Untag te' -> Untag (perform_subs te' binds)
  Ref var -> case lookup var binds of
    Nothing -> te
    Just te' -> te'
  App s e -> case e of
    Left tes -> te
    Right mods -> App s (Left (map 
                                (\(Rename v t) -> case lookup v binds of
                                  Nothing -> t
                                  Just te' -> te')
                              mods))
  CurrentTime -> CurrentTime 
  otherwise -> te

-- check if term is in the store. we strip doms because the query does not always provide one.
-- add option for anonymizing here. e.g. for Fact adoptions in adoption.eflint.
{--
p = PQuery
      (Tag
         (Count
            [Var "child" []]
            (When
               (Ref (Var "child" []))
               (Present
                  (App
                     "is-adopted" -- anonymized search AKA jst look for one is-adopted effect somewhere (the latest)
                     (Left
                        [App
                           "is-adopted"
                           (Right [Rename (Var "child" []) (Ref (Var "child" []))])])))))
         "adoptions")
--}
in_trans :: Bool -> Term -> [TransInfo] -> Maybe Bool
in_trans success t [] = Nothing
in_trans success t (trans:transs) = case trans of 
  TransInfo x0 store b ma tis -> case t of 
    And te te' -> in_trans_and_two_terms te te'
    Or te te' -> case in_trans success te (trans:transs) of
       Just True -> Just True
       otherwise -> in_trans success te' (trans:transs)
    Geq te te' -> in_trans_and_two_terms te te'
    Leq te te' -> in_trans_and_two_terms te te'
    Ge te te' -> in_trans_and_two_terms te te'
    Le te te' -> in_trans_and_two_terms te te'
    Sub te te' -> in_trans_and_two_terms te te'
    Add te te' -> in_trans_and_two_terms te te'
    Mult te te' -> in_trans_and_two_terms te te'
    Mod te te' -> in_trans_and_two_terms te te'
    Div te te' -> in_trans_and_two_terms te te'
    Eq te te' -> in_trans_and_two_terms te te'
    Neq te te' -> in_trans_and_two_terms te te'
    Not te -> case get_lit_from_term te of
      IntLit il -> traverse_store (not success) tags store (Product([(Int il, "nodom")]), get_dom_from_term t)
      StringLit sl -> traverse_store (not success) tags store (Product([(String sl, "nodom")]), get_dom_from_term t)
      App s args -> case args of
        Left tes -> traverse_store (not success) tags store (get_elem_from_args args, s)
        Right mods -> Just $ traverse_store_just_dom tags store s
      otherwise -> in_trans success t transs
    te -> case get_lit_from_term te of
      IntLit il -> traverse_store success tags store (Product([(Int il, "nodom")]), get_dom_from_term t)
      StringLit sl -> traverse_store success tags store (Product([(String sl, "nodom")]), get_dom_from_term t)
      App s args -> case args of
        Left tes -> traverse_store success tags store (get_elem_from_args args, s)
        Right mods -> Just $ traverse_store_just_dom tags store s
      otherwise -> in_trans success t transs
    where 
      tags = keys store
      in_trans_and_two_terms te te' = case in_trans success te (trans:transs) of
       Just True -> in_trans success te' (trans:transs)
       b -> b

traverse_store_just_dom :: [Tagged] -> Store -> DomId -> Bool
traverse_store_just_dom [] store d = False
traverse_store_just_dom ((el', d'):tags) store d
  | d == d' = True
  | otherwise = traverse_store_just_dom tags store d

-- returns an entry if it is a duty instance
traverse_store_find_duties :: [Tagged] -> [(Tagged, Assignment)] -> [(Term, QueryRes)]
traverse_store_find_duties vis [] = [] 
traverse_store_find_duties vis (((el,d), ass):store)
  | (el,d) `elem` vis = traverse_store_find_duties vis store
  | otherwise = case ass of 
      Unknown -> (tagged_to_term (el,d), QueryFailure):traverse_store_find_duties vis store
      HoldsFalse -> (tagged_to_term (el,d), QueryFailure):traverse_store_find_duties vis store
      HoldsTrue -> (tagged_to_term (el,d), QuerySuccess):traverse_store_find_duties vis store

-- search store to see if tag is present with expected value according to success of query.
traverse_store :: Bool -> [Tagged] -> Store -> Tagged -> Maybe Bool
traverse_store success [] store (el, d) = Nothing
traverse_store success ((el', d'):tags) store (el, d) = case success of
  False 
    | (el == el' || el == strip_doms el') && d == d' -> 
        Just (store M.! (el', d') == HoldsFalse || store M.! (el', d') == Unknown)
    | otherwise -> traverse_store success tags store (el, d)
  True
    | el == el' || (el == strip_doms el' && d == d') -> Just (store M.! (el', d') == HoldsTrue)
    | otherwise -> traverse_store success tags store (el, d)

-- strips doms to perform equals operation effectively
strip_doms :: Elem -> Elem
strip_doms el = case el of 
  String s -> el
  Int n -> el
  Product tags -> Product (map (\(el', d') -> (el', "nodom")) tags)

-- convert args to product elem
get_elem_from_args :: Arguments -> Elem
get_elem_from_args args = case args of
  Left tes -> Product(map (\t -> (term_to_elem t, get_dom_from_term t)) tes)
  Right mods -> Product(map (\(Rename var t) -> (term_to_elem t, get_dom_from_term t)) mods)

-- convert term to elem
term_to_elem :: Term -> Elem
term_to_elem t = case get_lit_from_term t of 
  IntLit n -> Int n
  StringLit s -> String s
  BoolLit b 
    | b -> String "True"
    | not b -> String "False"
  App s e -> case e of 
    Left (te:[]) -> term_to_elem te
    otherwise -> String "not what i expected"
  otherwise -> String "no literal here, supposed to be an error"

tagged_to_term :: Tagged -> Term
tagged_to_term (v,t) = case v of 
  String s    -> StringLit s
  Int i       -> IntLit i
  Product tes -> App t (Left (map tagged_to_term tes))

-- return domid of term 
get_dom_from_term :: Term -> DomId 
get_dom_from_term t = case t of
  Not te -> get_dom_from_term te
  And te te' -> get_dom_from_term te --kinda cumbersome here
  Or te te' -> get_dom_from_term te
  BoolLit b -> "nodom"
  Leq te te' -> get_dom_from_term te
  Geq te te' -> get_dom_from_term te
  Ge te te' -> get_dom_from_term te
  Le te te' -> get_dom_from_term te
  Sub te te' -> get_dom_from_term te
  Add te te' -> get_dom_from_term te
  Mult te te' -> get_dom_from_term te
  Mod te te' -> get_dom_from_term te
  Div te te' -> get_dom_from_term te
  IntLit n -> "nodom"
  StringLit s -> "nodom"
  Eq te te' -> get_dom_from_term te
  Neq te te' -> get_dom_from_term te
  Exists vars te -> get_dom_from_term te
  Forall vars te -> get_dom_from_term te
  Count vars te -> get_dom_from_term te
  Sum vars te -> get_dom_from_term te
  Max vars te -> get_dom_from_term te
  Min vars te -> get_dom_from_term te
  When te te' -> get_dom_from_term te'
  Present te -> get_dom_from_term te
  Violated te -> get_dom_from_term te
  Enabled te -> get_dom_from_term te
  Project te var -> get_dom_from_term te
  Tag te s -> s
  Untag te -> get_dom_from_term te
  Ref var -> case var of { Var s str -> s }
  App s e -> s
  CurrentTime -> "nodom" 

-- pretty much evaluate term but dumbed down version and also not quite accurate
get_lit_from_term :: Term -> Term
get_lit_from_term t = case t of
  Not te -> BoolLit(not (bool_value te))
  And te te' -> BoolLit(bool_value te && bool_value te')
  Or te te' -> BoolLit(bool_value te || bool_value te')
  BoolLit b -> BoolLit b
  Leq te te' -> BoolLit(int_value te <= int_value te')
  Geq te te' -> BoolLit(int_value te >= int_value te')
  Ge te te' -> BoolLit(int_value te > int_value te')
  Le te te' -> BoolLit(int_value te < int_value te')
  Sub te te' -> IntLit(int_value te - int_value te')
  Add te te' -> IntLit(int_value te + int_value te')
  Mult te te' -> IntLit(int_value te * int_value te')
  Mod te te' -> IntLit(mod (int_value te) (int_value te'))
  Div te te' -> IntLit(div (int_value te) (int_value te'))
  IntLit n -> IntLit n
  StringLit s -> StringLit s
  Eq te te' -> BoolLit(int_value te == int_value te')
  Neq te te' -> BoolLit(int_value te /= int_value te')
  Exists vars te -> get_lit_from_term te
  Forall vars te -> get_lit_from_term te
  Count vars te -> get_lit_from_term te
  Sum vars te -> get_lit_from_term te
  Max vars te -> get_lit_from_term te
  Min vars te -> get_lit_from_term te
  When te te' -> get_lit_from_term te'
  Present te -> get_lit_from_term te
  Violated te -> get_lit_from_term te
  Enabled te -> get_lit_from_term te
  Project te var -> get_lit_from_term te
  Tag te s -> get_lit_from_term te
  Untag te -> get_lit_from_term te
  Ref var -> Ref var -- is not accurate, needs sub --substitute_var(var) SUBSTITUTION IS DONE LATER
  App s e -> App s e -- is not accurate, needs sub make_substitutions_of vs e SUBSTITUTION IS DONE LATER
  CurrentTime -> CurrentTime

-- evaluate int value of term. must have numeric type term as input
int_value :: Term -> Int 
int_value t = case t of
  Sub te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 - n2
  Add te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 + n2
  Mult te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 * n2
  Mod te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> mod n1 n2
  Div te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> div n1 n2
  IntLit n -> n
  otherwise -> -99999999 -- temporary placeholder to avoid throwing errors. should probably use nothing

-- evaluate bool value of term. must have boolean type term as input
bool_value :: Term -> Bool 
bool_value t = case t of
  Not te -> case bool_value te of 
    b1 -> not b1
  And te te' -> case (bool_value te, bool_value te') of 
    (b1, b2) -> b1 && b2
  Or te te' -> case (bool_value te, bool_value te') of 
    (b1, b2) -> b1 || b2
  BoolLit b -> b
  Leq te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 <= n2
  Geq te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 >= n2
  Ge te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 > n2
  Le te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 < n2
  Eq te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 == n2
  Neq te te' -> case (int_value te, int_value te') of 
    (n1, n2) -> n1 /= n2
  otherwise -> False -- temporary placeholder to avoid throwing errors. should probably use nothing
  
-- get all declarations in spec using configuration in some state.
get_spec_from_cfg :: Config -> Spec 
get_spec_from_cfg cfg = case cfg of 
  Config cfg_spec cfg_state rest_transitions rest_duties -> cfg_spec

concatingvis :: [Tagged] -> Spec -> [IO ()] -> Int -> IO ()
concatingvis tags spc [] i = case i of 
  1 -> putStrLn("None\n")
  otherwise -> putStrLn("\nNo other violations\n")
concatingvis tags spc (io:ios) i = putStrLn("  " ++ show i ++ ") " ++ (ppTagged $ head tags)) 
                              >> putStr("  violation condition(s): ") >> concating_term sub_dutyconds 
                              >> io >> concatingvis (tail tags) spc ios (i+1)
  where 
      te' = tagged_to_term $ head tags
      d = get_dom_from_term te'
      conds = conds_instance d spc
      sub_dutyconds = (map (\te'' -> perform_subs te'' $ dutybinds) conds)
      -- sub_dutyvars = map (\bind -> sub_var bind) $ dutybinds
      dutybinds = zip (dutydom) $ dutyargs
      dutydom = case dutydecl of 
        Just (TypeSpec _ dom _ _ _ _ _) -> case dom of
          Products vars -> vars
          otherwise -> []
        Nothing -> []
      dutyargs = get_args te'
      dutydecl = find_decl spc $ d

concating_term :: [Term] -> IO ()
concating_term [] = putStrLn("")
concating_term (t:ts) = putStr(ppTerm t) >> concating_term ts

concating :: [(Term, QueryRes)] -> Spec -> [IO ()] -> Int -> IO ()
concating compls spc [] i = case i of 
  1 -> putStrLn("None")
  otherwise -> putStrLn("\nNo other compliant duties")
concating compls spc (io:ios) i = case (snd $ head compls) of 
  QuerySuccess -> putStrLn("  " ++ show i ++ ") " ++ (ppTerm te')) 
                >> putStr("  violation conditions: ") >> concating_term sub_dutyconds
                >> io >> concating (tail compls) spc ios (i+1)
    where 
      te' = fst $ head compls
      d = get_dom_from_term te'
      conds = conds_instance d spc
      sub_dutyconds = (map (\te'' -> perform_subs te'' $ dutybinds) conds)
      -- sub_dutyvars = map (\bind -> sub_var bind) $ dutybinds
      dutybinds = zip (dutydom) $ dutyargs
      dutydom = case dutydecl of 
        Just (TypeSpec _ dom _ _ _ _ _) -> case dom of
          Products vars -> vars
          otherwise -> []
        Nothing -> []
      dutyargs = get_args te'
      dutydecl = find_decl spc $ d
  otherwise -> concating (tail compls) spc ios i

conds_instance :: DomId -> Spec -> [Term]
conds_instance d s = case find_decl s d of 
  Nothing -> []
  Just (TypeSpec kind b c d e f g) -> case kind of
    Duty (DutySpec a b c d) -> d
    otherwise -> g

op_compliance :: Options -> Phrase -> Explorer -> QueryRes -> IO ()
op_compliance opts p exp QueryFailure = case r1 of
  0 -> case dr1 of
    0 -> verbosity opts TestMode (putStrLn (""))
    otherwise -> case ddec of
      Nothing -> verbosity opts TestMode (putStrLn (""))
      Just ddecl -> verbosity opts TestMode (putStrLn ("    transition: #" ++ (show dr1) ++ " => #" 
                  ++ (show dr2) ++ "\n    phrase: " ++ ppPhrase dphr ++ "\n    by derivation:\n      " ++ ppDecl ddecl))
    where Language.EFLINT.Explain.Explanation (dphr, dr1, dr2, ddec) = search_derivations False p (reverse $ getTrace exp)
  otherwise -> case dec of 
    Nothing -> verbosity opts TestMode (putStrLn ("    transition: #" ++ (show r1) ++ " => #" ++ (show r2)
             ++ "\n    phrase: " ++ ppPhrase phr ++ "\n    by postulation."))
    Just decl -> verbosity opts TestMode (putStrLn ("    transition: #" ++ (show r1) ++ " => #" ++ (show r2)
               ++ "\n    phrase: " ++ ppPhrase phr ++ "\n    by effect:\n      " ++ ppDecl decl))
  where Language.EFLINT.Explain.Explanation (phr, r1, r2, dec) = search_trace False p (reverse $ getTrace exp)
op_compliance opts p exp QuerySuccess = case r1 of
  0 -> case dr1 of
    0 -> verbosity opts Default (putStrLn (""))
    otherwise -> case ddec of
      Nothing -> verbosity opts Default (putStrLn (""))
      Just ddecl -> verbosity opts Default (putStrLn ("    transition: #" ++ (show dr1) ++ " => #"
                   ++ (show dr2) ++ "\n    phrase: " ++ ppPhrase dphr ++ "\n    by derivation:\n      " ++ ppDecl ddecl))
    where Language.EFLINT.Explain.Explanation (dphr, dr1, dr2, ddec) = search_derivations True p (reverse $ getTrace exp)
  otherwise -> case dec of 
    Nothing -> verbosity opts Default (putStrLn ("    transition: #" ++ (show r1) ++ " => #" ++ (show r2)
               ++ "\n    phrase: " ++ ppPhrase phr ++ "\n    by postulation."))
    Just decl -> verbosity opts Default (putStrLn ("    transition: #" ++ (show r1) ++ " => #" ++ (show r2)
               ++ "\n    phrase: " ++ ppPhrase phr ++ "\n    by effect:\n      " ++ ppDecl decl))
  where Language.EFLINT.Explain.Explanation (phr, r1, r2, dec) = search_trace True p (reverse $ getTrace exp)

